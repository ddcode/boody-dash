import qs from 'qs'
import axios from 'axios'
import store from '../store'

export const doFetch = (url, body, method, options) => {
    const debugMode =
        typeof process.env.NODE_ENV !== 'undefined' &&
        process.env.NODE_ENV === 'development'
    if (typeof url !== `string`) {
        return new Promise(() => {
            throw new Error(`url must be set for all requests!`)
        })
    }

    if (!localStorage.getItem('token')) {
        store.commit('forceLogout')
    }

    const defaultOptions = {
        url: url,
        method: 'get',
        maxRedirects: 1,
        baseURL: process.env.VUE_APP_API_URL,
        validateStatus: function(status) {
            return status
        },
        // headers: {
        //     Authorization: `Bearer ${localStorage.getItem('token')}`,
        // },
    }

    if (typeof body === 'object') {
        defaultOptions.data =
            body instanceof FormData ? body : qs.stringify(body)
    }

    if (typeof method === 'string') {
        defaultOptions.method = method
    }

    if (typeof options === 'object') {
        for (const key in options) {
            defaultOptions[key] = options[key]
        }
    }

    defaultOptions.loader && store.commit('showLoader')

    if (debugMode) {
        /* eslint-disable */
        console.groupCollapsed(`doRequest: ${defaultOptions.method.toUpperCase()} ${url}`);
        console.debug(defaultOptions);
        console.trace('__callee');
        console.groupEnd();
        /* eslint-enable */
    }

    return axios.request(defaultOptions).then(response => {
        if (response.status.toString().match(/(10[0-3]|20[0-8]|226)/)) {
            return response
        }

        if (response.status.toString().match(/30[0-8]/)) {
            // follow request
        }

        if (response.status.toString().match(/40[13]/)) {
            store.commit('forceLogout')
        }

        const errorMessage =
            response.data && response.data.message
                ? response.data.message
                : `${response.status}: ${response.statusText}!`

        if (debugMode) {
            // eslint-disable-next-line
                console.warn(errorMessage, response);
        }
        throw new Error(errorMessage)
    })
}
