import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Logs from './views/Logs.vue'
import People from './views/People.vue'
import Actions from './views/Actions.vue'
import Devices from './views/Devices.vue'
import Locations from './views/Locations.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            icon: 'home',
            component: Home,
            inSidebar: true,
        },
        {
            path: '/actions',
            name: 'Actions',
            icon: 'offline_bolt',
            component: Actions,
            inSidebar: true,
        },
        {
            path: '/actions/create',
            name: 'ActionsCreate',
            component: Actions,
            inSidebar: false,
        },
        {
            path: '/actions/:id/edit',
            name: 'ActionsEdit',
            component: Actions,
            inSidebar: false,
        },
        {
            path: '/devices',
            name: 'Devices',
            icon: 'phonelink',
            component: Devices,
            inSidebar: true,
        },
        {
            path: '/devices/create',
            name: 'DevicesCreate',
            component: Devices,
            inSidebar: false,
        },
        {
            path: '/devices/:id/edit',
            name: 'DevicesEdit',
            component: Devices,
            inSidebar: false,
        },
        {
            path: '/locations',
            name: 'Locations',
            icon: 'place',
            component: Locations,
            inSidebar: true,
        },
        {
            path: '/locations/create',
            name: 'LocationsCreate',
            component: Locations,
            inSidebar: false,
        },
        {
            path: '/locations/:id/edit',
            name: 'LocationsEdit',
            component: Locations,
            inSidebar: false,
        },
        {
            path: '/people',
            name: 'People',
            icon: 'people',
            component: People,
            inSidebar: true,
        },
        {  
            path: '/people/create',
            name: 'PeopleCreate',
            icon: 'people',
            component: People,
            inSidebar: false,
        },
        {
            path: '/people/:id/edit',
            name: 'PeopleEdit',
            icon: 'people',
            component: People,
            inSidebar: false,
        },
        {
            path: '/logs',
            name: 'Logs',
            icon: 'history',
            inSidebar: true,
            component: Logs
        },
        {
            path: '/logout',
            name: 'Logout',
            icon: 'exit_to_app',
            inSidebar: true,
        }
    ]
})
