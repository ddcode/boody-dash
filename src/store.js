import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        drawer: localStorage.getItem('drawerOption')
        ? JSON.parse(localStorage.getItem('drawerOption'))
        : true,
    },
    getters: {
        getDrawerState(state) {
            return state.drawer
        },
    },
    mutations: {
        toggleDrawer: state => {
            state.drawer = !state.drawer
            localStorage.setItem('drawerOption', state.drawer)
        },
    },
})
